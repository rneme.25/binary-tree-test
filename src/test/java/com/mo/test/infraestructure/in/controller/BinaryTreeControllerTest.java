package com.mo.test.infraestructure.in.controller;

import com.mo.test.BinaryTreeTestApplication;
import com.mo.test.infraestructure.in.request.Request;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;

import java.util.Arrays;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = BinaryTreeTestApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class BinaryTreeControllerTest {

    @Autowired
    private BinaryTreeController binaryTreeController;

    @Test
    void createTree() {
        // Llamar metodo de controller
        var response = binaryTreeController.createTree(getCreateBinaryTree());

        // Validar respuesta
        assertEquals(response.getStatusCode().value(), HttpStatus.OK.value());
    }

    @Test
    void findAncestor() {
        // Llamar metodo de controller
        var response = binaryTreeController.findAncestor(getCreateBinaryTreeAncestor());

        // Validar respuesta
        assertEquals(response.getStatusCode().value(), HttpStatus.OK.value());
        assertEquals(Objects.requireNonNull(response.getBody()).getAncestor(), 6);
    }

    @Test
    void findAncestorError() {
        // Llamar metodo de controller
        var response = binaryTreeController.findAncestor(getCreateBinaryTreeAncestorError());

        // Validar respuesta con error
        assertEquals(response.getStatusCode().value(), HttpStatus.BAD_REQUEST.value());
    }

    private Request getCreateBinaryTree(){
        return Request.builder().nodeList(Arrays.asList (6,2,3,4,5,7,8,9,10)).build();
    }

    private Request getCreateBinaryTreeAncestor(){
        return Request.builder()
                        .nodeList(getCreateBinaryTree().getNodeList())
                        .valueNodeOne(3)
                        .valueNodeTwo(8)
                        .build();
    }

    private Request getCreateBinaryTreeAncestorError(){
        return Request.builder()
                        .nodeList(getCreateBinaryTree().getNodeList())
                        .valueNodeOne(30)
                        .valueNodeTwo(8)
                        .build();
    }

}