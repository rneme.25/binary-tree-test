package com.mo.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BinaryTreeTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(BinaryTreeTestApplication.class, args);
	}

}
