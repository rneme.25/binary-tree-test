package com.mo.test.infraestructure.in.controller;

import com.mo.test.infraestructure.in.request.Request;
import com.mo.test.infraestructure.in.response.Response;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;

import static com.mo.test.commons.helper.ConstantsHelper.TAG_CREATE_TREE;
import static com.mo.test.commons.helper.ConstantsHelper.TAG_FIND_ANCESTOR_TREE;


public interface BinaryTreeControllerDocumentation {


    @Tag(name = TAG_CREATE_TREE)
    @Operation(summary = "Crear arbol binario", description = "Crea un arbol binario segun los valores enviados", tags = {TAG_CREATE_TREE},
            requestBody = @RequestBody(content = @Content(
                    schema = @Schema(implementation = Request.class),
                    examples = {
                        @ExampleObject(
                                name = "Crear arbol binario",
                                description = "Realiza creación de arbol binario",
                                value = "{\"nodeList\": [2, 1, 3]}"
                        )
                    }
                )
            )
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Transacción exitosa"),
            @ApiResponse(responseCode = "400", description = "Error en campos", content = @Content )
    })
    ResponseEntity<Response> createTree(@org.springframework.web.bind.annotation.RequestBody Request request);


    @Tag(name = TAG_FIND_ANCESTOR_TREE)
    @Operation(summary = "Buscar ancestro", description = "Busca el ancestro común más cercano del arbol binario ingresado", tags = {TAG_FIND_ANCESTOR_TREE},
            requestBody = @RequestBody(content = @Content(
                    schema = @Schema(implementation = Request.class),
                    examples = {
                        @ExampleObject(
                                name = "Buscar antecesor",
                                description = "Realiza busqueda de antecesor del arbol binario",
                                value = "{\"valueNodeOne\": 15,\"valueNodeTwo\": 3,\"nodeList\": [10,2,3,9,15]}"
                        )
                    }
                )
            )
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Transacción exitosa"),
            @ApiResponse(responseCode = "400", description = "Error en campos", content = @Content )
    })
    ResponseEntity<Response> findAncestor(@org.springframework.web.bind.annotation.RequestBody Request request);

}
