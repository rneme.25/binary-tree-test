package com.mo.test.infraestructure.in.request;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Request {

    @Schema(description = "Listado de nodos, el primer nodo ingresado sera la raiz del arbol", example = "[2, 1, 3]")
    //@Pattern(regexp = "[0-9]+", message = "nodeList debe ser numerico")
    @NotNull(message = "El campo nodeList no puede estar vacio o nulo")
    private List<Integer> nodeList;

    @Schema(description = "Valor de nodo uno a buscar", example = "1")
    //@NotNull(message = "El campo valueNodeOne no puede estar vacio o nulo")
    private Integer valueNodeOne;

    @Schema(description = "Valor de nodo dos a buscar", example = "3")
    //@NotNull(message = "El campo valueNodeTwo no puede estar vacio o nulo")
    private Integer valueNodeTwo;

}
