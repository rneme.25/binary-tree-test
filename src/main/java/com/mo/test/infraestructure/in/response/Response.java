package com.mo.test.infraestructure.in.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Response {

        @Schema(description = "Listado de nodos ingresados", example = "2, 1, 3", required = true)
        private List<Integer> nodeList;

        @Schema(description = "Arbol binario generado", example = "{\"tree\": {\"nodeList\": [2,1,3], \"root\": {\"value\": 2,\"left\": {\"value\": 1,\"left\": null,\"right\": null},\"right\": {\"value\": 3,\"left\": null,\"right\": null}}}}", required = true)
        private Object tree;

        @Schema(description = "Antecesor común más cercano del arbol binario ingresado", example = "2", required = true)
        private Integer ancestor;

        @Schema(description = "Mensaje respuesta de solicitud", example = "Transaccion exitosa", required = true)
        private String message;
}
