package com.mo.test.infraestructure.in.controller;

import com.mo.test.application.in.ITreeBinaryCase;
import com.mo.test.commons.helper.ConstantsHelper;
import com.mo.test.commons.log.LogController;
import com.mo.test.infraestructure.in.request.Request;
import com.mo.test.infraestructure.in.response.Response;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.mo.test.commons.enums.OperationType.CREATE_TREE_BINARY;
import static com.mo.test.commons.enums.OperationType.GET_ANCESTOR_TREE_BINARY;


@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping(ConstantsHelper.PROJECT_PATH)
public class BinaryTreeController implements BinaryTreeControllerDocumentation{

    private final ITreeBinaryCase iTreeBinaryCase;

    /**
     * Metodo para crear arbol binario
     * @param request Objeto de solicitud
     * @return ResponseEntity<Response>
     */
    @LogController(optType = CREATE_TREE_BINARY)
    @PostMapping(value = ConstantsHelper.SERVICE_CREATE_NODE_PREFIX)
    public ResponseEntity<Response> createTree(@RequestBody Request request){

        var response = iTreeBinaryCase.createBinaryTree(request);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    /**
     * Metodo para obtener antecesor mas cercano de arbol binario ingresado
     * @param request Objeto de solicitud
     * @return ResponseEntity<Response>
     */
    @LogController(optType = GET_ANCESTOR_TREE_BINARY)
    @PostMapping(value = ConstantsHelper.SERVICE_GET_ANCESTOR_TREE_BINARY_PREFIX)
    public ResponseEntity<Response> findAncestor(@RequestBody Request request) {

        var response = iTreeBinaryCase.findAncestor(request);
        return new ResponseEntity<>(response, response.getAncestor().equals(0)?HttpStatus.BAD_REQUEST:HttpStatus.OK);
    }


}
