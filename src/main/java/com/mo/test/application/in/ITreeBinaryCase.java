package com.mo.test.application.in;

import com.mo.test.infraestructure.in.request.Request;
import com.mo.test.infraestructure.in.response.Response;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;

@Validated
public interface ITreeBinaryCase {

    Response createBinaryTree(@Valid Request request);

    Response findAncestor(@Valid Request request);

}
