package com.mo.test.application.service;

import com.mo.test.application.in.ITreeBinaryCase;
import com.mo.test.domain.entities.BinaryTree;
import com.mo.test.domain.entities.Node;
import com.mo.test.domain.service.ITreeBinary;
import com.mo.test.infraestructure.in.request.Request;
import com.mo.test.infraestructure.in.response.Response;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.validation.Valid;

@Slf4j
@AllArgsConstructor
@Service
public class TreeBinaryCaseService implements ITreeBinaryCase {

    private final ITreeBinary iTreeBinary;

    @Override
    public Response createBinaryTree(Request request) {
        // Realizar mapping a un objeto de dominio
        var binaryTree = BinaryTree.builder().nodeList(request.getNodeList()).build();
        // Llamar logica de creacion de arbol binario
        var responseBinaryTree = iTreeBinary.createBinaryTree(binaryTree);
        // Retornar respuesta y realizar mapping a un objeto de infraestructura
        return Response.builder().tree(responseBinaryTree).build();
    }

    @Override
    public Response findAncestor(@Valid Request request) {

        // Validar request
        validatedRequestAncestor(request);

        // Realizar mapping a un objeto de dominio
        var binaryTree = BinaryTree.builder().nodeList(request.getNodeList()).build();
        var node1 = Node.builder().value(request.getValueNodeOne()).build();
        var node2 = Node.builder().value(request.getValueNodeTwo()).build();

        // Llamar logica de creacion de arbol binario
        var responseAncestor = iTreeBinary.findAncestor(binaryTree, node1, node2);
        String message = responseAncestor.equals(0) ? "No existen nodo(s) de referencia ingresado(s)" : "OK";

        // Retornar respuesta y realizar mapping a un objeto de infraestructura
        return Response.builder().ancestor(responseAncestor).message(message).build();
    }

    private void validatedRequestAncestor(Request request){
        if (request.getValueNodeOne()==null || request.getValueNodeTwo()==null)
            throw new RuntimeException("Campos valueNodeOne o valueNodeTwo no pueden ser vacios o nulos");
    }

}
