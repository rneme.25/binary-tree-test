package com.mo.test.commons.properties;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import static com.mo.test.commons.helper.ConstantsHelper.SPRING_CONFIG_PREFIX;

@Data
@ConfigurationProperties(prefix = SPRING_CONFIG_PREFIX)
public class GlobalProperties {

    private String name;
    private String version;
    private Integer restPort;
    private String root;

}
