package com.mo.test.commons.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;


import static com.mo.test.commons.helper.ConstantsHelper.COMPONENT_PROPERTIES_PREFIX;


@Data
@ConfigurationProperties(prefix = COMPONENT_PROPERTIES_PREFIX)
public class ComponentProperties {



}