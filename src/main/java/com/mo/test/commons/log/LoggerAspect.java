package com.mo.test.commons.log;


import com.mo.test.commons.enums.OperationType;
import com.mo.test.infraestructure.in.request.Request;
import com.mo.test.infraestructure.in.response.Response;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Objects;
import java.util.UUID;


@Slf4j
@Aspect
@Component
@AllArgsConstructor
public class LoggerAspect {


    /**
     * Logica para imprimir logs de la capa controller
     */
    @Around("@annotation(LogController)")
    public Object logStart(ProceedingJoinPoint point) {

        Object objResponse;
        String idTransaction = UUID.randomUUID().toString();

        // 1. Obtenemos el tipo de operacion agregada en la anotacion "LogController"
        OperationType optType = ((MethodSignature) point.getSignature()).getMethod().getAnnotation(LogController.class).optType();

        // 2. Obtenemos los parametros del metodo para imprimir solo Request de entrada
        Arrays.stream(point.getArgs()).forEach(param -> LogUtil.getLogStart((Request) param, optType, idTransaction));

        // 3. Continuar flujo y obtener respuesta
        try {
            objResponse = point.proceed();
        } catch (Throwable e) {
            objResponse = getObjResponse(e);
        }

        // 4. Imprimir logs de salida
        Response response = (Response) ((ResponseEntity) objResponse).getBody();
        LogUtil.getLogEnd(Objects.requireNonNull(response), optType, idTransaction);

        return objResponse;
    }


    // Obtener objeto de respuesta en caso de error
    private Object getObjResponse(Throwable e){

        log.error("Error:[{}]", e.getMessage());
        return new ResponseEntity<>(Response.builder().build(), HttpStatus.BAD_REQUEST);

    }


}
