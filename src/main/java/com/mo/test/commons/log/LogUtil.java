package com.mo.test.commons.log;

import com.mo.test.commons.enums.OperationType;
import com.mo.test.infraestructure.in.request.Request;
import com.mo.test.infraestructure.in.response.Response;
import lombok.extern.slf4j.Slf4j;


import static com.mo.test.commons.helper.ConstantsHelper.*;


@Slf4j
final class LogUtil {

    private LogUtil() { }

    static void getLogStart(Request request, OperationType optType, String idTransaction){
        log.info(LONG_LINE);
        log.info(LBL_START, optType, idTransaction);
        log.info(LBL_REQUEST, request);
    }

    static void getLogEnd(Response response, OperationType optType, String idTransaction){
        log.info(LBL_RESPONSE, response);
        log.info(LBL_END, optType, idTransaction);
        log.info(LONG_LINE);
    }

}
