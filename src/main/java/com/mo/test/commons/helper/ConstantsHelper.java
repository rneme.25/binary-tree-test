package com.mo.test.commons.helper;

public final class ConstantsHelper {

    private ConstantsHelper(){}

    // Yml propiedades generales
    public static final String PROJECT_PATH = "${spring.application.root}";
    public static final String SPRING_CONFIG_PREFIX = "spring.application";
    public static final String SERVICE_CREATE_NODE_PREFIX = "${spring.application.services.rest.createTree.path}";
    public static final String SERVICE_GET_ANCESTOR_TREE_BINARY_PREFIX = "${spring.application.services.rest.getAncestorNode.path}";
    public static final String COMPONENT_PROPERTIES_PREFIX = "properties";

    // Documentación
    public static final String TAG_CREATE_TREE = "1) Crear arbol binario";
    public static final String TAG_FIND_ANCESTOR_TREE = "2) Buscar antecesor";


    //Logs
    public static final String LBL_REQUEST = "REQUEST:{}";
    public static final String LBL_RESPONSE = "RESPONSE:{}";
    public static final String LBL_START = "************** INICIA SERVICIO {} [{}] **************";
    public static final String LBL_END = "************** FINALIZA SERVICIO {} [{}] **************";
    public static final String LONG_LINE = "------------------------------------------------";


}
