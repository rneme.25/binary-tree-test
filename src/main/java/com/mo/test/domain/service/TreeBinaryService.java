package com.mo.test.domain.service;

import com.mo.test.domain.entities.BinaryTree;
import com.mo.test.domain.entities.Node;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


@Slf4j
@AllArgsConstructor
@Service
public class TreeBinaryService implements ITreeBinary {

    /**
     * Metodo para crear arbol binario
     */
    @Override
    public BinaryTree createBinaryTree(BinaryTree binaryTree) {

        // Agregar listado de nodos y generar arbol binario
        binaryTree.getNodeList().forEach(binaryTree::add);
        return binaryTree;
    }

    /**
     * Metodo para buscar antecesor mas cercano comun
     */
    @Override
    public Integer findAncestor(BinaryTree binaryTree, Node nodeOne, Node nodeTwo) {

        // Agregar listado de nodos y generar arbol binario
        binaryTree.getNodeList().forEach(binaryTree::add);

        //Validar si nodos existen
        var existNode = BinaryTree.isNodePresent(binaryTree.getRoot(), nodeOne.getValue(), nodeTwo.getValue());

        if (!existNode){
            log.error("No existen nodos de referencia ingresados");
           return 0;
        }

        // Buscar antecesor
        return binaryTree.lowestCommonAncestor(nodeOne.getValue(), nodeTwo.getValue()).getValue();
    }


}
