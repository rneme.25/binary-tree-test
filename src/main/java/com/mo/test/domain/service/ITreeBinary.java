package com.mo.test.domain.service;

import com.mo.test.domain.entities.BinaryTree;
import com.mo.test.domain.entities.Node;


public interface ITreeBinary {

    BinaryTree createBinaryTree(BinaryTree binaryTree);

    Integer findAncestor(BinaryTree binaryTree, Node nodeOne, Node nodeTwo);

}
