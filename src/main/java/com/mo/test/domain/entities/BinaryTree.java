package com.mo.test.domain.entities;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
public class BinaryTree {

    private List<Integer> nodeList;
    private Node root;

    // Inicializar desde nodo raiz
    public void add(Integer value) {
        root = add(root, value);
    }

    // Aplica recursividad para agregar nodos
    private Node add(Node currentNode, Integer value) {

        // Si nodo actual es nulo, insertar nuevo nodo en esa posicion
        if (currentNode == null) {
            return new Node(value);
        }

        // Si nodo es menor al actual, ir por nodo izquierdo
        if (value < currentNode.value) {
            currentNode.left = add(currentNode.left, value);
        }// Si nodo es mayor al actual, ir por nodo derecho
        else if (value > currentNode.value) {
            currentNode.right = add(currentNode.right, value);
        } else {
            // Si ya existe valor, retorna nodo actual
            return currentNode;
        }

        return currentNode;
    }

    // Inicializar desde nodo raiz
    public Node lowestCommonAncestor(Integer valueOne, Integer valueTwo) {
        return lowestCommonAncestor(root, valueOne, valueTwo);
    }

    // Aplica recursividad para buscar valor de nodos
    private Node lowestCommonAncestor(Node currentNode, Integer valueOne, Integer valueTwo) {

        // Si nodo actual es nulo, es una hoja o nodo terminal
        if (currentNode == null) {
            return null;
        }

        // Si valor nodo actual coincide con nodo izquierdo o derecho retorne nodo actual
        if (currentNode.value.equals(valueOne) || currentNode.value.equals(valueTwo)) {
            return currentNode;
        }

        // Buscar ancestro comun por izquierda y derecha
        Node leftNodeAncestor = lowestCommonAncestor(currentNode.left, valueOne, valueTwo);
        Node rightNodeAncestor = lowestCommonAncestor(currentNode.right, valueOne, valueTwo);

        // Si se encuentra ancestro en nodos izquierda y derecha
        if (leftNodeAncestor != null && rightNodeAncestor != null) {
            return currentNode;
        }

        // Valide si el nodo izquierdo o derecho es ancestro
        return (leftNodeAncestor != null) ? leftNodeAncestor : rightNodeAncestor;
    }

    private void printInOrder(Node node) {
        if (node != null) {
            printInOrder(node.left);
            System.out.print("-" + node.value);
            printInOrder(node.right);
        }
    }


    public static boolean isNodePresent(Node node, Integer lookupLeftValue, Integer lookupRightValue) {

        // Buscar valor izquierdo
        boolean existLeftValue = lookupLeftValue < node.value ? isNodePresent(node.left, lookupLeftValue) : isNodePresent(node.right, lookupLeftValue);
        if(!existLeftValue)
            return false;

        // Buscar valor derecho
        return lookupRightValue < node.value ? isNodePresent(node.left, lookupRightValue) : isNodePresent(node.right, lookupRightValue);

    }

    private static boolean isNodePresent(Node node, Integer searchValue) {
        // Validar raiz
        if (node == null) {
            return false;
        }

        // Si existe valor
        if (node.value == searchValue) {
            return true;
        }

        // Retorna verdadero si valor se encuentra en el nodo izquierdo o derecho
        return searchValue < node.value ? isNodePresent(node.left, searchValue) : isNodePresent(node.right, searchValue);

    }

}
