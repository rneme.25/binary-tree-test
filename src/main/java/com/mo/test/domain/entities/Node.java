package com.mo.test.domain.entities;

import lombok.*;


@Data
@Builder
@AllArgsConstructor
public class Node {

    Integer value;
    Node left;
    Node right;

    Node(Integer value) {
        this.value = value;
        right = null;
        left = null;
    }

}
