# Binary - Tree - Test

Componente encargado de:

1. Crear un arbol binario.
2. Dado un arbol binario y dos nodos retornar el ancestro común más cercano.

## Requisitos del sistema
- [ ] Git. Si aún no tiene instalado Git, puede seguir las instrucciones en [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git).
- [ ] Maven 3.5+. Spring Boot es compatible con Apache Maven 3.3 o superior. Si aún no tiene instalado Maven, puede seguir las instrucciones en [Maven](https://maven.apache.org).
- [ ] Java 11. Si aún no ha instalado JAVA dirijase al link [JDK 11](https://www.oracle.com/co/java/technologies/javase/jdk11-archive-downloads.html)
- [ ] Verifique instalación del JAVA ejecutando el comando "java -version" desde consola. 

## Agregar archivos

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/rneme.25/binary-tree-test.git
git branch -M main
git push -uf origin main

```

## Nombre
Proyecto prueba MO.

## Descripción
Crear arbol binario y consultar antecesor.

## Instalación
- [ ] Ubicarse en la ruta donde se alojara el proyecto. 

- [ ] Clonar proyecto, ejecutar comando :

```
git clone https://gitlab.com/rneme.25/binary-tree-test.git
```

- [ ] Limpiar y empaquetar, ejecutar comando:
```
mvn clean install
```

- [ ] Desplegar servicio, ejecutar comando:
```
mvn spring-boot:run
```
- [ ] Una vez levantado el servicio, se podra acceder al mismo por el puerto 8080

## Uso
Para poder acceder a la documentación y consumo del servicio ingresar a la [Documentacion API](http://localhost:8080/binary-tree/swagger-ui/index.html), la cual tiene información detallada y ejemplos para consumir el servicio expuesto implementada con [Swagger](https://swagger.io/docs/specification/about/).
Realice los siguientes pasos:
1. Ingrese a la ruta [Documentacion API](http://localhost:8080/binary-tree/swagger-ui/index.html)
2. Identifique la operación a consumir, despliegue menu y dar click en el boton [Try it out].
3. Verificar request a enviar y/ó modificar.
4. Dar click en el boton [Execute].
5. Validar respuesta.

## Opcional
Instale el IDE de su preferencia y que este sea compatible con spring boot, se recomienda [Intellij IDEA](https://www.jetbrains.com/es-es/idea/download/#section=windows).
Una vez instalado se debe importar proyecto y ejecutarlo dando click en la opción Run, el cual desaplegara el servicio automaticamente en el puerto 8080.